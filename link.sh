link vim       ~/.vim
link gitconfig ~/.gitconfig
link zshrc     ~/.zshrc
link tmux      ~/.tmux.conf
