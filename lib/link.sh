link vim       ~/.vim
link gitconfig ~/.gitconfig
link zshrc     ~/.zshrc
link tmux      ~/.tmux.conf

if [ -d ~/.config ] # any XDG-based locations
then
    link vim ~/.config/nvim
fi
