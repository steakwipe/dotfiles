# all of these assume the proper things are passed to them
# mostly because you're not supposed to use them directly

link() {
    if test -d "${2}" && ! test -h "${2}"; then
        mv "${2}" "_${2}"
        echo "Moved ${2} to _${2}, as it was conflicting, and not a symlink."
    fi
    ln -nsf "${PWD}/${1}" "${2}"
}

git_name() { # we will send the result into $1
    gitfile="${2##*/}" # remove everything up to just after the last /
    repo_name="${gitfile%.git}" # remove trailing .git if any
    eval "$1=\${repo_name}"
}

git_dirty() { # returns whether or not a git stash is needed
    # it is needed when we have a new (staged) file ('A')
    # or when we have a modified (staged or not) file ('M')
    git -C $1 status --porcelain | grep -Fq -e 'A' -e 'M'
    return $?
}
