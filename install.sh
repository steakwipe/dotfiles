#!/bin/sh

install_dir="$(dirname $0)"
cd "${install_dir}"

. lib/config.sh
. lib/functions.sh

# PWD is set-up, now we can call all the bits
. lib/update.sh # clone and update any external repos
. lib/ignore.sh # make sure all external repos are not tracked
. lib/link.sh   # link all da files
