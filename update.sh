git fetch
git rebase --autostash --quiet
[ -x ./r_update.sh ] && ./r_update.sh

. ./config.sh # update the config, in case it was modified upstream

for repo in $repos
do
    git_name loc "$repo"
    [ -d "$loc" ] || git clone "$repo" "$loc"
    git -C "./$loc" fetch
    git -C "./$loc" rebase --autostash --quiet
    [ -x "./$loc/r_update.sh" ] && "./$loc/r_update.sh"
done
