Toasts' Dotfiles
================

[![license]](LICENSE.md)

Personal Stuff
--------------

A quick way to setup a usable environment for me. Feel free to use it for yourself, (under the terms of the license). Any modifications should be saved using git-stash or git commit (install.sh will fetch and rebase).

Uses the following projects:
- [toasty-zsh][toast-zsh], a standalone zsh configuration. (developed separately since it's useful on its own as well)
- [vim-toastpack][vim-toastpack], a set of packages I personally use. See details at that repository.

Quick setup:
- clone repository (anywhere, ~/dotfiles is recommended)
- cd into the directory
- ./install.sh

Requirements:
- Any POSIX-compatible shell (as a `/bin/sh` provider)

Things for which a configuration is provided:
- git
- tmux
- vim 8 or neovim
- zsh

[license]: https://img.shields.io/github/license/5pacetoast/dotfiles.svg
[toast-zsh]: https://github.com/5paceToast/toasty-zsh "toasty-zsh"
[vim-toastpack]: https://github.com/5paceToast/vim-toastpack "Toast's Vim Package"
